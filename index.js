require("dotenv").config();
const express = require("express");
const db = require("./db/connect");
const PORT = process.env.PORT || 3000;

const app = express();
app.use(express.json());

let pool = db.connect();

app.get("/api", (req, res) => {
  res.json({ message: "hello everyone" });
});

app.get("/api/cars", (req, res) => {
  let text = "SELECT * FROM cars";
  db.query(text, [], (err, result) => {
    if (!err) {
      res.json(result.rows);
      return;
    }
    res.status(500).send({ message: "Some error occurred while retrieving cars." });
    return console.error(err);
  });
  pool.end;
});

app.get("/api/cars/:id", (req, res) => {
  const id = req.params.id;

  let text = "SELECT * FROM cars WHERE id = $1";
  let params = [id];

  pool.query(text, params, (err, result) => {
    if (!err) {
      res.json(result.rows);
      return;
    }
    res
      .status(500)
      .send({ message: `Some error occurred while retrieving cars at id ${id}` });
    return console.error(err);
  });
  pool.end;
});

app.post("/api/cars/", (req, res) => {
  let body = req.body;
  let isValid = true;

  let params = Object.values(body).map((value) => {
    if (!value) {
      isValid = false;
    }
    return value;
  });

  if (isValid) {
    let text = `INSERT INTO cars
    (name, maker, type, seater, model_year, wheel_drive) 
    VALUES($1, $2, $3, $4, $5, $6) RETURNING *`;

    pool.query(text, params, (err, result) => {
      if (!err) {
        res.json(result.rows);
        return;
      }
      res.status(500).send({ message: `unable to post a new car`, err });
      return console.error(err);
    });
    pool.end;
  } else {
    res.status(400).send({ message: "bad request" });
  }
});

app.put("/api/cars/:id", (req, res) => {
  const putData = generatePutQueryString(req.body);
  const id = req.params.id;
  putData.params.push(id);
  const idParamIndex = putData.params.length;

  const text = `UPDATE cars SET ${putData.query} WHERE id = $${idParamIndex}`;
  const params = putData.params;

  pool.query(text, params, (err, result) => {
    if (!err) {
      res.json(result.rows);
      return;
    }
    res.status(500).send({ message: `unable to edit car info`, err });
    return console.error("DB error", err);
  });
  pool.end;
});

app.delete("/api/cars/:id", (req, res) => {
  const id = req.params.id;

  const text = `DELETE FROM cars WHERE id = $1`;
  const params = [id];

  pool.query(text, params, (err, result) => {
    if (!err) {
      res.json(result.rows);
      return;
    }
    res.status(500).send({ message: `unable to delete car at index ${id}`, err });
    return console.error(err);
  });
  pool.end;
});

function generatePutQueryString(body) {
  let entries = Object.entries(body);
  let params = [];
  let query = entries
    .map((entry, index) => {
      params.push(entry[1]);
      return `${entry[0]}=$${index + 1}`;
    })
    .join(",");

  return {
    query: query,
    params: params,
  };
}

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
