var { Pool } = require("pg");
var connectionString = process.env.DB_URL;

let poolGlobal;
function connect() {
  let pool = new Pool({ connectionString });
  pool.connect(function (err) {
    if (err) {
      return console.error("could not connect to postgres", err);
    }
    pool.query('SELECT NOW() AS "theTime"', function (err, result) {
      if (err) {
        return console.error("error running query", err);
      }
      console.log("connection established on", result.rows[0].theTime);
      pool.end;
    });
  });
  poolGlobal = pool;
  return pool;
}

function query(text, params, callback) {
  return poolGlobal.query(text, params, callback);
}

module.exports = { connect, query };
